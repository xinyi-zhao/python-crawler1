import time
from selenium import webdriver
from selenium.webdriver.common.by import By
import pymysql
import re

kwargs = {
        "host": "localhost",
        "port": 3306,
        "user": "root",
        "password": "123456",
        "database": "douban",
        "charset": "utf8"
    }
db = pymysql.connect(**kwargs)
cur = db.cursor()

options = webdriver.ChromeOptions()
options.add_argument('--headless')
driver=webdriver.Chrome(executable_path='/home/xinyi/chromedriver',options=options)
driver.maximize_window()
driver.get(url="https://book.douban.com/top250?start=0")

def getpage():
    data = driver.find_elements(By.XPATH, '//tr[@class="item"]')
    # print(data)
    for item in data:
        name = item.find_element(By.XPATH, './/div[@class="pl2"]/a').text
        info = item.find_element(By.XPATH, './/p[@class="pl"]').text
        score = item.find_element(By.XPATH, './/span[@class="rating_nums"]').text
        comments = item.find_element(By.XPATH, './/span[@class="pl"]').text
        try:
            desc = item.find_element(By.CLASS_NAME, "inq").text
        except:
            desc = "本书无描述"
        # print(name, info, score, comments,desc)
        data=cleandata(name,info,score,comments,desc)
        time.sleep(0.1)
        writedb(data)

def cleandata(name,info,score,comments,desc):
    # 百年孤独 [哥伦比亚] 加西亚·马尔克斯 / 范晔 / 南海出版公司 / 2011-6 / 39.50元 9.3 ( 402061人评价 ) 魔幻现实主义文学代表作
    name1=name.strip()
    res=info.split('/')
    author1=res[0].strip()
    if len(res[1].strip())<=4:
        publisher1=res[2].strip()
    else:
        publisher1=res[1].strip()
    price1=res[-1]
    score1=score.strip()
    comments1=re.search(r'\d+',comments).group()
    desc1 = desc.strip()
    data=[name1,author1,publisher1,price1,score1,comments1,desc1]
    return data
#
def writedb(data):
    sql = "insert into doubantop250 values(%s,%s,%s,%s,%s,%s,%s)"
    try:
        cur.execute(sql, data)
    except Exception as e:
        db.rollback()
    db.commit()

while True:
    getpage()
    try:
        driver.find_element(By.XPATH, '//span[@class="next"]/a').click()
    except Exception:
        driver.quit()
        cur.close()
        db.close()
        break





